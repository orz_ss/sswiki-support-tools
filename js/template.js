/*!
 * template.js by orz, MIT license
 * Based on doT.js:
 *   2011, Laura Doktorova, https://github.com/olado/doT
 *   Licensed under the MIT license.
 */

(function() {
	"use strict";

	var Template = {
		version: '1.0.1',
		templateSettings: {
			evaluate:    /\{\{([\s\S]+?(\}?)+)\}\}[\r\n]?/g,
			interpolate: /\{\{=([\s\S]+?)\}\}/g,
			conditional: /\{\{(\?)?\?\s*([\s\S]*?)\s*\}\}[\r\n]?/g,
			iterate:     /\{\{~\s*(?:\}\}|([\s\S]+?)(?:\s*\:\s*([\w$]+)(?:\s*,\s*([\w$]+))?)?\s*\}\})[\r\n]?/g,
			varname:     'data',
			itemname:    'value',
			strip:       false
		},
		template: undefined, //fn, compile template
		compile:  undefined  //fn, for express
	}, global;

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = Template;
	} else if (typeof define === 'function' && define.amd) {
		define(function(){return Template;});
	} else {
		global = (function(){ return this || (0,eval)('this'); }());
		global.Template = Template;
	}

	var skip = /$^/;

	function unescape(code) {
		return code.replace(/\\('|\\)/g, "$1").replace(/[\r\t\n]/g, ' ');
	}

	Template.template = function(str, c, def) {
		var sid = 0, indv;
		c = c || Template.templateSettings;

		str = ("var out='" +
				(c.strip ? str
					.replace(/(^|[\r\n])\t* +| +\t*([\r\n]|$)/g, ' ')
					.replace(/[\r\n\t]|\/\*[\s\S]*?\*\//g, '')
				: str)
			.replace(/'|\\/g, '\\$&')
			.replace(c.interpolate || skip, function(m, code) {
				return "'+(" + unescape(code) + ")+'";
			})
			.replace(c.conditional || skip, function(m, elsecase, code) {
				return elsecase ?
					(code ? "';}else if(" + unescape(code) + "){out+='" : "';}else{out+='") :
					(code ? "';if(" + unescape(code) + "){out+='" : "';}out+='");
			})
			.replace(c.iterate || skip, function(m, iterate, vname, iname) {
				if (!iterate) return "';} } out+='";
				sid++;
				indv = iname || "i" + sid;
				vname = vname || c.itemname;
				iterate = unescape(iterate);
				return "';var arr"+sid+"="+iterate+";if(arr"+sid+"){var "+vname+","+indv+"=-1,l"+sid+"=arr"+sid+".length-1;while("+indv+"<l"+sid+"){"
					+vname+"=arr"+sid+"["+indv+"+=1];out+='";
			})
			.replace(c.evaluate || skip, function(m, code) {
				return "';" + unescape(code) + "out+='";
			})
			+ "';return out;")
			.replace(/\n/g, '\\n').replace(/\t/g, '\\t').replace(/\r/g, '\\r')
			.replace(/(^|[\s;{}])out\+='';/g, '$1').replace(/\+''/g, '')
			.replace(/(^|[\s;{}])out\+=''\+/g,'$1out+=');

		try {
			return new Function(c.varname, str);
		} catch (e) {
			if (typeof console !== 'undefined') console.log("Could not create a template function: " + str);
			throw e;
		}
	};

	Template.compile = function(tmpl, def) {
		return Template.template(tmpl, null, def);
	};
}());
