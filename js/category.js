$(function() {
	var gen = window.Generator;

	var splitter = /\s*\n\s*/g;

	gen.createData = function() {
		var parents = $('[name=parent]').map(function() {
			return $.trim($(this).val()).replace(/^(?:Category|カテゴリ):/, "") || null;
		}).get();
		var genres = $('[name=genre]:checked').map(function(){ return $(this).val(); }).get();
		var cats = parents.concat(genres);
		var title = $.trim($('[name=title]').val());

		var errors = [];
		if (title == "") errors.push("タイトルがありません。");

		return {
			title: "Category:" + title,
			summary: $.trim($('[name=summary]').val()),
			text: $.trim($('[name=text]').val()),
			links: getLinks(),
			categories: $.map(cats, function(item){ return "[[Category:" + item + "]]"; }).join("\n"),

			validationErrors: errors
		};
	};

	function getLinks() {
		var links = $.trim($('[name=links]').val());
		if (links) {
			return links.split(splitter);
		}
	}

	var pastTitle = "";
	$('[name=title]').change(function() {
		var title = $.trim($(this).val());

		if (title) {
			var summary = $('[name=summary]').val();

			if ($.trim(summary) == "") {
				summary = "'''Category:" + title + "''' は、" +
					($('#fanfic').prop('checked') ? "『" + title + "』の二次創作" : "「" + title + "」の") +
					"[[SSスレッド]]に関するカテゴリ。";

				$('[name=summary]').val(summary);
			} else {
				summary = summary.replace("'''Category:" + pastTitle + "'''", "'''Category:" + title + "'''");

				if ($('#fanfic').prop('checked')) {
					summary = summary.replace("『" + pastTitle + "』の二次創作", "『" + title + "』の二次創作");
				} else {
					summary = summary.replace("「" + pastTitle + "」の", "「" + title + "」の");
				}

				$('[name=summary]').val(summary);
			}
			
			pastTitle = title;
		}
	});

	$('#fanfic').click(function() {
		var summary = $('[name=summary]').val();

		if (pastTitle && summary) {
			if (this.checked) {
				$('[name=summary]').val(summary.replace("「" + pastTitle + "」の", "『" + pastTitle + "』の二次創作"));
			} else {
				$('[name=summary]').val(summary.replace("『" + pastTitle + "』の二次創作", "「" + pastTitle + "」の"));
			}
		}
	});

	var parentCount = 0;
	$('#parents').on("change", ':last', function() {
		$('#parent_template').clone().removeAttr("id").appendTo(this.parentElement);
		
		if (++parentCount == 2)
			$('#crossover').prop("checked", true);
	});

	$('#append_header').click(function() {
		var textarea = $('[name=text]');
		textarea.val(textarea.val() + "\n== 見出し ==\n");
	});
});
