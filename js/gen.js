$(function() {
	var gen = {};

	var render = Template.compile($.trim($('#template').html()));

	$('#generate_button').click(function() {
		var data = gen.createData();

		var normalizedTitle = data.title.replace(/[#<>\[\]|{}]/g, '_');

		if (/<.+>|\[\[.+\]\]|{{.+}}/.test(data.title)) {
			data.title = "<nowiki>" + data.title + "</nowiki>";
		}

		$('#result').text(render(data));

		$('#alerts').empty();
		if (data.validationErrors) {
			for (var i = 0, length = data.validationErrors.length; i < length; i++) {
				gen.alert(data.validationErrors[i]);
			}
		}

		$('#edit_link').children('a')
			.attr("href", "http://ss.vip2ch.com/ss?title=" + encodeURIComponent(normalizedTitle) + "&action=edit")
			.text(normalizedTitle + " の編集ページを開く");

		$('#source_section').show();
	});

	$('.toggle_visible').click(function(e) {
		$(this).toggleClass("uk-icon-caret-right uk-icon-caret-down")
			.next().slideToggle("fast");
		e.preventDefault();
	});

	gen.alert = function (message) {
		$('#alerts').append('<div class="uk-alert uk-alert-warning uk-icon-warning-sign">' + message + '</div>');
	};

	gen.info = function (message) {
		$('#alerts').append('<div class="uk-alert">' + message + '</div>');
	};

	window.Generator = gen;
});
