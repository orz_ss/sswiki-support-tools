$(function() {
	var gen = window.Generator;

	var splitter = /\s*\n\s*/g;
	
	gen.createData = function() {
		var genres = $('[name=genre]:checked').map(function(){ return $(this).val(); }).get();
		var status = $('[name=status]:checked').val();
		var cats = genres.concat(status);
		var title = $.trim($('[name=title]').val());

		var errors = [];
		if (title == "") errors.push("タイトルがありません。");

		return {
			title: title,
			genre: $.map(cats, function(item){ return "[[:Category:" + item + "|" + item + "]]"; }).join("、"),
			narrator: getNarrator(),
			searchword: $.trim($('[name=searchword]').val()) || '',
			remarks: getRemarks(),
			collector: $.trim($('[name=collector]').val()) || '',
			summary: $.trim($('[name=summary]').val()),
			outline: $.trim($('[name=outline]').val()),
			characters: getCharacters(),
			currenturl: $.trim($('[name=currenturl]').val()) || '',
			pasturl: getPastUrl(),
			categories: $.map(cats, function(item){ return "[[Category:" + item + "]]"; }).join("\n"),

			validationErrors: errors
		};
	};

	function getNarrator() {
		var narrator = $('[name=narrator]').val();
		if (narrator == "-") return $.trim($('#narrator_other').val());
		else return narrator;
	}

	function getRemarks() {
		return "[[" + $('[name=from]:checked').val() + "]]発";
	}

	function getCharacters() {
		var $expl = $('[name=expl]');
		var charas = $('[name=name]').map(function(i) {
			var name = $.trim($(this).val());
			if (name) return {
				'name': name,
				'expl': $.trim($expl.eq(i).val()).replace(splitter, "\n: ")
			};
		}).get();
		
		return charas;
	}

	function getPastUrl() {
		var pasturl = $.trim($('[name=pasturl]').val());
		if (pasturl) {
			return pasturl.split(splitter);
		}
	}

	$('[name=title]').change(function() {
		var val = $(this).val();
		if (val.indexOf("安価") != -1 || val.indexOf("コンマ") != -1)
			$('#anchor').prop("checked", true);
	});

	$('[name=narrator]').change(function() {
		if ($(this).val() == "-") $('#narrator_other').show();
		else $('#narrator_other').hide();
	});

	$('#characters').on("change", '[name=name]:last', function() {
		$('#character_template').clone().removeAttr("id").appendTo('#characters');
	});
});
